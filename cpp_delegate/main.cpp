#include <iostream>
#include <memory>
#include "delegate.h"

//a simple callback
void print_input(int in)
{
	std::cout << "Calling print_input with: " << in << std::endl;
}
void other_input(const int& in)
{
	std::cout << "A Thing.webm\n";
}

//a simple class to prove the delegates work with classes.
struct simple_class : public delegate_v3::flag_wrapper
{
	//note, we keep a delegate_flag so if this object is destroyed we can disable the delegate.
	//this does have to be set via some other easy method, either by extorior assignment, or inherting a common easy assignment.
	delegate_v3::del_destruct_flag del_flag;

	//a member to prove that each object can be unique.
	int num;
	simple_class() : num(0) {}
	simple_class(int n) :num(n) {}
	//a basic method we wish to call 
	void obj_method(int n) const
	{
		std::cout << "Calling simple_class.obj_method with: " << n << " private num: " << num << std::endl;
	}
	void obj_method_test(const int& n)
	{
		std::cout << "It works?\n";
	}
};
void func_ref(const int&){}
void func_normal(int&){}
int main()
{
	//creat our basic delegate.
	//delegate<return_type, args...>
	delegate_v3::delegate<void, int> delegate;

	//proving that lambdas now work with the delegate system.
	int y = 1;
	delegate.add([&y](int x) {std::cout << "lambda with : " << x << " " << y << std::endl; });
	//lambdas dont have to match the exact signature; so long as the types they request can be cast around properly they'll be accepted.
	delegate.add([](const int& x) {std::cout << "Lambdas work too\n"; std::cout << "This can't decay to a func_ptr due to ref vs nonref\n"; });

	//register a free-standing function.
	//we add the function to the delegate by supplying the function as a func_ptr to the template.
	delegate.add(&print_input);
	delegate.add([](auto x)->void {other_input(x); });

	//register a delegate tied to a class; 
	//note we're doing it in an enclosing block to show how del_destruct_flag works.
	{
		simple_class simple(12);
		//we want to make an object callback, so we preface with that.
		//then we want to bind a method; so we specify the method we wish to bind after specifying the object we're bound to.
		//our class must publicly inherit from flag_wrapper<size_t> and specify the amount of delegates we want to use.
		//this function will auto take care of assigning flags.
		//if we wish to manually manage our flags specify .func_self_managed<func_ptr>(); instead.
		delegate.add(&simple, &simple_class::obj_method);
		delegate.add(&simple,&simple_class::obj_method_test);
		std::cout << "Inside scope with simple_class\n";
		//call our delegate showing our currently bound funcitons
		delegate(1234);
	}
	std::cout << "\nOutside scope with simple_class\n";
	//outside of the scope of simple; we should no longer see it being called.
	delegate(5678);

	//small test to make sure returning data from a delegate works properly.
	delegate_v3::delegate<int, int> delegate_test;
	delegate_test.add([](int x) {return x; });

	auto vec_pack = std::vector<int>{ 12354 };
	auto rets = delegate_test(vec_pack);
	for (auto i : rets)
	{
		std::cout << i << '\n';
	}
	auto vec_tuple_pack = std::vector<std::tuple<int>>{ {12345667} };
	auto rets_t = delegate_test(vec_tuple_pack);
	for (auto i : rets_t)
	{
		std::cout << i << '\n';
	}
}