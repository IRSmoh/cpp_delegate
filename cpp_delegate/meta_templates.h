#pragma once
namespace delegate_v3
{
	template<typename _type, bool status>
	struct status_type : std::integral_constant<bool, status>
	{
		using type = _type;
	};
	template<typename ... args>
	struct arg_pack {};

	template<typename type>
	struct func_arg_solver_t
	{
		using is_function = std::false_type;
	};

	template<typename ret_type, typename ... _args>
	struct func_arg_solver_t<ret_type(*)(_args...)>
	{
		using ret = ret_type;
		using args = std::tuple<_args...>;
		using is_function = std::true_type;
		using func_ptr_type = ret_type(*)(_args...);
	};
	template<typename ret_type, typename obj_type, typename ... _args>
	struct func_arg_solver_t<ret_type(obj_type::*)(_args...)>
	{
		using ret = ret_type;
		using args = std::tuple<_args...>;
		using obj = obj_type;
		using is_function = std::true_type;
		//used for finding out a valid casting target for lambda like objects
		using func_ptr_type = ret_type(*)(_args...);
		using method_ptr_type = ret_type(obj_type::*)(_args...);
	};
	template<typename ret_type, typename obj_type, typename ... _args>
	struct func_arg_solver_t<ret_type(obj_type::*)(_args...) const>
	{
		using ret = ret_type;
		using args = std::tuple<_args...>;
		using obj = obj_type;
		using is_function = std::true_type;
		//used for finding out a valid casting target for lambda like objects
		using func_ptr_type = ret_type(*)(_args...);
		using method_ptr_type = ret_type(obj_type::*)(_args...) const;
	};

	template<size_t ... ind, typename fill_type>
	decltype(auto) filled_tuple_f(std::index_sequence<ind...>, fill_type fill_data)
	{
		//since this makes fuck all sense looking at it.....
		//we use the 'cheat' of the bracket list initialized expansion of a variadic template...
		//we can sub out whatever our results are for a different piece of data (which we do)
		//and make a tuple out of that data; that all coincides with the same type/data.
		//thus you can make a tuple of 10 elements of x type easily now.
		//the (void) is a hack to silence the compiler from complaining about an unused variable; this thing's all statically compiled anyway.. so who cares.
		return std::make_tuple(((void)ind, fill_data)...);
	}

	//creates a std::tuple<> of element count size; and sets all of the types to fill_type.
	template<typename fill_type, typename ... types>
	using filled_tuple = decltype(filled_tuple_f(std::index_sequence_for<types...>(), fill_type()));

	template<typename _type, bool status>
	struct remove_const_ref_impl
	{
		using type = _type;
	};
	template<typename _type>
	struct remove_const_ref_impl<_type, true>
	{
		using type = std::add_lvalue_reference_t<std::remove_const_t<std::remove_reference_t<_type>>>;
	};
	template<typename _type>
	using remove_const_ref_t = typename remove_const_ref_impl<_type, std::is_reference<_type>::value>::type;

	template<typename _type, bool status>
	struct remove_volatile_ref_impl
	{
		using type = _type;
	};
	template<typename _type>
	struct remove_volatile_ref_impl<_type, true>
	{
		using type = std::add_lvalue_reference_t<std::remove_volatile_t<std::remove_reference_t<_type>>>;
	};
	template<typename _type>
	using remove_volatile_ref_t = typename remove_volatile_ref_impl<_type, std::is_reference<_type>::value>::type;

	template<typename _type>
	using remove_cv_ref_t = remove_volatile_ref_t<remove_const_ref_t<_type>>;

	template<typename _type, bool status>
	struct remove_const_ptr_impl
	{
		using type = _type;
	};
	template<typename _type>
	struct remove_const_ptr_impl<_type, true>
	{
		using type = std::add_pointer_t<std::remove_const_t<std::remove_pointer_t<_type>>>;
	};
	template<typename _type>
	using remove_const_ptr_t = typename remove_const_ptr_impl<_type, std::is_pointer<_type>::value>::type;

	template<typename _type, bool status>
	struct remove_volatile_ptr_impl
	{
		using type = _type;
	};
	template<typename _type>
	struct remove_volatile_ptr_impl<_type, true>
	{
		using type = std::add_pointer_t<std::remove_volatile_t<std::remove_pointer_t<_type>>>;
	};
	template<typename _type>
	using remove_volatile_ptr_t = typename remove_volatile_ptr_impl<_type, std::is_pointer<_type>::value>::type;

	template<typename _type>
	using remove_cv_ptr_t = remove_volatile_ptr_t<remove_const_ptr_t<_type>>;

	template<typename _ty1, typename _ty2>
	struct is_safe_to_func_cast
	{
		//although its UB to cast a function; its safe to use the result so long as the cast'd function can parse the arguments correctly.
		//if the arg type matches (object, ref, pointer) then its safe to use if the qualifers are correct.
		//e.g. int -> int is always valid.
		//_ty1 = {int&} and _ty2 = {const int&} is fine assuming the func cast is like: (void(*)(int&))(void(*)(const int&))
		//similarly; _ty1 = {const int&} and _ty2 = {int&} should fail since we are losing the constness of the function. sure it may be able to run without a glitch but theres a reason the qualifiers existed.
		//pointers undergo a similar stance; but {int const*} is not dealt with. 
		//{const int*} can be checked vs {int*} however.
		constexpr static bool value =
			std::is_same<_ty1, _ty2>::value ||
			std::is_same<_ty1, std::remove_volatile_t<_ty2>>::value ||
			std::is_same<_ty1, std::remove_const_t<_ty2>>::value ||
			std::is_same<_ty1, std::remove_cv_t<_ty2>>::value ||
			std::is_same<_ty1, remove_const_ref_t<_ty2>>::value ||
			std::is_same<_ty1, remove_volatile_ref_t<_ty2>>::value ||
			std::is_same<_ty1, remove_cv_ref_t<_ty2>>::value ||
			std::is_same<_ty1, remove_const_ptr_t<_ty2>>::value ||
			std::is_same<_ty1, remove_volatile_ptr_t<_ty2>>::value ||
			std::is_same<_ty1, remove_cv_ptr_t<_ty2>>::value;
		using type = std::integral_constant<bool, value>;
	};
	template<typename ..._tys1, typename ... _tys2>
	struct is_safe_to_func_cast<std::tuple<_tys1...>, std::tuple<_tys2...>>
	{
	private:
		using true_vals = std::tuple<std::integral_constant<bool, is_safe_to_func_cast<_tys1, _tys2>::value>...>;
	public:
		using type = typename std::is_same<true_vals, decltype(filled_tuple_f(std::make_index_sequence<std::tuple_size<true_vals>::value>(), std::true_type()))>::type;
		constexpr static bool value = type::value;
	};

	//checks if _ty1 passed to a funtion expecting _ty2 is valid.
	template<typename _ty1, typename _ty2>
	struct is_more_restrictive
	{
	private:
		using type1 = std::remove_reference_t<_ty1>;
		using type2 = std::remove_reference_t<_ty2>;
	public:
		constexpr static bool value = is_safe_to_func_cast<type1, type2>::value;
		using type = std::integral_constant<bool, value>;
	};
	//pass in your args from a function via std::tuple and this will determine if _all_ types given are either the same or can be safely cast to their more restricted version provided by the second std::tuple.
	//e.g.....
	//std::tuple<int&,double&> and std::tuple<const int&, double> would be true; since const has more restrictions and copies can't affect the original data.
	//std::tuple<const int&> and std::tuple<const volatile int&> is false along with a 'lengthy' compiler error if the sizes of the tuples miss match.
	template<typename ... _tys1, typename ... _tys2>
	struct is_more_restrictive<std::tuple<_tys1...>, std::tuple<_tys2...>>
	{
		//this can be obtuse to read for awhile.

		//we find out how all of our supplied types (via tuple) compose; and if they follow the regular rules.
		//normally we'd merge all the true/falses via fold expresssions but vc++ doesn't support that yet; only clang.
		//so instead we get to understand that std::true/false_type are actually std::integral_constant<bool,true/false>
		//so we make a tuple of integral constants; then we compare if its the same to another tuple of constants
		//we gen the tuple by expanding an index sequence and abusing the comma operator to expand the pack but disregard its default value and instead supply our own type (std::true_type) in this case.
	private:
		//since we are passed 2 parameter packs; anytime we expand one we have to expand both.. the way filled_tuple<> works requires only one pack; so instead we have to basically recreate it.
		//in any case; we can use std::tuple_size to grab how many elements are in this tuple and refill a tuple based on that.
		using true_vals = std::tuple<std::integral_constant<bool, is_more_restrictive<_tys1, _tys2>::value>...>;
	public:
		using type = typename std::is_same<true_vals, decltype(filled_tuple_f(std::make_index_sequence<std::tuple_size<true_vals>::value>(), std::true_type()))>::type;
		constexpr static bool value = type::value;
	};
	template<typename derived_type, typename derived_from>
	struct is_derived
	{
	private:
		static std::true_type test(derived_from const&) {}
		static std::false_type test(...) {}
	public:
		using type = decltype(test(std::declval<derived_type>()));
		static constexpr bool value = type::value;
	};

	template<typename, typename = void>
	struct has_lambda_mem_fnc : status_type<std::false_type, false> {};
	//if lambda has ::operator() then type will be the mem_fnc_ptr type. else type is std::false_type.
	template<typename lambda>
	struct has_lambda_mem_fnc<lambda, std::enable_if_t<std::is_member_function_pointer<decltype(&lambda::operator())>::value, void>> : status_type<decltype(&lambda::operator()), true> {};

	template<typename lambda, typename arg_pack, typename = void>
	struct has_auto_lambda_mem_fnc_impl : status_type<std::false_type, false> {};

	//check to see if lambda has a valid operator()
	template<typename lambda, typename ... args>
	struct has_auto_lambda_mem_fnc_impl<lambda, arg_pack<args...>,
		std::enable_if_t<std::is_member_function_pointer<decltype(&lambda::template operator()<args...>)>::value, void>>
		: status_type<decltype(&lambda::template operator()<args...>), true>
	{};
	//if lambda has ::operator()<args...> then type will be the mem_fnc_ptr type. else type is std::false_type.
	template<typename lambda, typename ... args>
	struct has_auto_lambda_mem_fnc : has_auto_lambda_mem_fnc_impl<lambda, arg_pack<args...>> {};
}